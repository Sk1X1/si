package com.sk1x1.castlerecognition;

/**
 * Class with constants
 *
 * @author Jiri PLACEK
 */
public class Constants {

    /** API key */
    static String API_KEY = "o-MXh72tTpHe4l9PxQgu5fpXlQxV4LfbsR1G5OVLujCR";
    /** Version date */
    static String VERSION_DATE = "2019-04-19";
    /** Classifier ID */
    static String CLASSIFIER_ID = "DefaultCustomModel_133003818";
    /** Bundle string for image Uri */
    static String BUNDLE_IMAGEURI = "imageUri";
}
