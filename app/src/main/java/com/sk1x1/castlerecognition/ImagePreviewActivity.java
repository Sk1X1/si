package com.sk1x1.castlerecognition;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ibm.watson.developer_cloud.service.security.IamOptions;
import com.ibm.watson.developer_cloud.visual_recognition.v3.VisualRecognition;
import com.ibm.watson.developer_cloud.visual_recognition.v3.model.ClassifiedImages;
import com.ibm.watson.developer_cloud.visual_recognition.v3.model.ClassifyOptions;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.Locale;

/**
 * Class for image recognition.
 *
 * @author Jiri PLACEK, Hana Svobodova
 */
public class ImagePreviewActivity extends AppCompatActivity {

    private VisualRecognition visualRecognition;
    private ProgressBar progressBar;
    private Uri imageUri;
    TextView resultText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_preview);

        progressBar = findViewById(R.id.progressBar);
        ImageView imageView = findViewById(R.id.imagePreview);
        Button bSend = findViewById(R.id.bSend);
        resultText = findViewById(R.id.resultText);

        progressBar.setVisibility(View.GONE);

        if(getIntent().getExtras() != null && getIntent().getExtras().getString(Constants.BUNDLE_IMAGEURI) != null) {
            imageUri = Uri.parse(getIntent().getExtras().getString(Constants.BUNDLE_IMAGEURI));
            imageView.setImageURI(imageUri);
        }

        bSend.setOnClickListener(view -> {
            try {
                new ImageRecognize(visualRecognition, progressBar, bSend, imageUri).execute(getContentResolver().openInputStream(imageUri));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });

        initRecognition();
    }

    /**
     * Initialize visual recognition.
     */
    private void initRecognition() {
        IamOptions options = new IamOptions.Builder()
                .apiKey(Constants.API_KEY)
                .build();

        visualRecognition = new VisualRecognition(Constants.VERSION_DATE, options);
    }

    /**
     * Async task class which will get send castle image to recognize to IBM cloud.
     */
    private static class ImageRecognize extends AsyncTask<InputStream, Void, ClassifiedImages> {

        private VisualRecognition visualRecognition;
        private WeakReference<ProgressBar> progressBar;
        private WeakReference<Button> resultText;
        private Uri uri;

        ImageRecognize(VisualRecognition visualRecognition, ProgressBar progressBar, Button resultText, Uri uri){
            this.visualRecognition = visualRecognition;
            this.progressBar = new WeakReference<>(progressBar);
            this.resultText = new WeakReference<>(resultText);
            this.uri = uri;
        }

        @Override
        protected void onPreExecute() {
            progressBar.get().bringToFront();
            progressBar.get().setVisibility(View.VISIBLE);
        }

        @Override
        protected ClassifiedImages doInBackground(InputStream... stream) {
            ClassifyOptions classifyOptions = new ClassifyOptions.Builder()
                    .imagesFile(stream[0])
                    .imagesFilename(uri.getLastPathSegment())
                    .threshold(0.6f)
                    .classifierIds(Collections.singletonList(Constants.CLASSIFIER_ID))
                    .build();

            return visualRecognition.classify(classifyOptions).execute();
        }

        @Override
        protected void onPostExecute(ClassifiedImages result) {
            progressBar.get().setVisibility(View.GONE);
            String text;
            if (result.getImages().get(0).getClassifiers().get(0).getClasses().size() > 0) {
                text = result.getImages().get(0).getClassifiers().get(0).getClasses().get(0).getClassName() + " ("
                        + String.format(Locale.getDefault(), "%.2f", result.getImages().get(0).getClassifiers().get(0).getClasses().get(0).getScore() * 100) + " %)";
            }
            else {
                text = "Hrad nebyl rozpoznán";
            }
            resultText.get().setEnabled(false);
            resultText.get().setText(text);
        }
    }
}
