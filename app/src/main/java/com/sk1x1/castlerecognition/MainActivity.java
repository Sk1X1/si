package com.sk1x1.castlerecognition;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

/**
 * Main class.
 *
 * @author Jiri PLACEK, Hana SVOBODOBA
 */
public class MainActivity extends AppCompatActivity {

    private final int SELECT_PICTURE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bGallery = findViewById(R.id.bGallery);

        bGallery.setOnClickListener(v -> openGallery());
    }

    /**
     * Opens activity with photo preview.
     */
    private void showImageFromGallery(Uri imageUri) {
        Intent intent = new Intent(MainActivity.this, ImagePreviewActivity.class);
        intent.putExtra(Constants.BUNDLE_IMAGEURI, imageUri.toString());
        startActivity(intent);
    }

    /**
     * Opens android gallery.
     */
    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            switch (requestCode) {
                case SELECT_PICTURE:
                    showImageFromGallery(data.getData());
                    break;
            }
        }
    }
}

